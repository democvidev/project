<?php

namespace App\Entity;

use App\Repository\CommentsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommentsRepository::class)
 */
class Comments
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $commentContent;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $rgpd;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $emailAuthor;

    /**
     * @ORM\Column(type="datetime")
     */
    private $commentCreatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=Posts::class, inversedBy="comments")
     */
    private $post;

    public function __toString()
    {
        return $this->commentContent;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommentContent(): ?string
    {
        return $this->commentContent;
    }

    public function setCommentContent(string $commentContent): self
    {
        $this->commentContent = $commentContent;

        return $this;
    }

    public function getRgpd(): ?bool
    {
        return $this->rgpd;
    }

    public function setRgpd(?bool $rgpd): self
    {
        $this->rgpd = $rgpd;

        return $this;
    }

    public function getEmailAuthor(): ?string
    {
        return $this->emailAuthor;
    }

    public function setEmailAuthor(string $emailAuthor): self
    {
        $this->emailAuthor = $emailAuthor;

        return $this;
    }

    public function getCommentCreatedAt(): ?\DateTimeInterface
    {
        return $this->commentCreatedAt;
    }

    public function setCommentCreatedAt(\DateTimeInterface $commentCreatedAt): self
    {
        $this->commentCreatedAt = $commentCreatedAt;

        return $this;
    }

    public function getPost(): ?Posts
    {
        return $this->post;
    }

    public function setPost(?Posts $post): self
    {
        $this->post = $post;

        return $this;
    }
}
