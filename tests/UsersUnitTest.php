<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Users;

class UsersUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new Users();

        $user->setEmail('true@test.com')
             ->setLogin('login')
             ->setPassword('password');             

        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getLogin() === 'login');
        $this->assertTrue($user->getPassword() === 'password');

    }

    public function testIsFalse()
    {
        $user = new Users();

        $user->setEmail('true@test.com')
             ->setLogin('login')
             ->setPassword('password');             

        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getLogin() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
    }

    public function testIsEmpty()
    {
        $user = new Users();                     

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getLogin());
        $this->assertEmpty($user->getPassword());
    }
}
