# Projet 

Création et déploiement d'un blog Symfony


## Requirements

PHP 7.3
Node JS
Yarn
Composer

## Installation

Aller dans votre répertoire de travail

```bash
git clone https://gitlab.com/democvidev/project.git

composer install

yarn install --force

php bin/console doctrine:migrations:migrate

php -S 127.0.0.1:8000 -t public

yarn run build
```

## Usage

Page d'accueil du projet http://127.0.0.1:8000/

Page d'accueil phpmyadmin http://127.0.0.1:8080 (user:root , psw:root)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)