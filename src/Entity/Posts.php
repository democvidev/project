<?php

namespace App\Entity;

use App\Repository\PostsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=PostsRepository::class)
 */
class Posts
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postTitle;

    /**
     * @Gedmo\Slug(fields={"postTitle"})
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $postSlug;

    /**
     * @ORM\Column(type="text")
     */
    private $postContent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postImage;

    /**
     * @var \DateTime $postCreatedAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $postCreatedAt;

    /**
     * @var \DateTime $postUpdatedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $postUpdatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="posts")
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity=Categories::class, inversedBy="posts")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity=Comments::class, mappedBy="post")
     */
    private $comments;

    public function __construct()
    {
        $this->category = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPostTitle(): ?string
    {
        return $this->postTitle;
    }

    public function setPostTitle(string $postTitle): self
    {
        $this->postTitle = $postTitle;

        return $this;
    }

    public function getPostSlug(): ?string
    {
        return $this->postSlug;
    }

    // public function setPostSlug(string $postSlug): self
    // {
    //     $this->postSlug = $postSlug;

    //     return $this;
    // }

    public function getPostContent(): ?string
    {
        return $this->postContent;
    }

    public function setPostContent(string $postContent): self
    {
        $this->postContent = $postContent;

        return $this;
    }

    public function getPostImage(): ?string
    {
        return $this->postImage;
    }

    public function setPostImage(string $postImage): self
    {
        $this->postImage = $postImage;

        return $this;
    }

    public function getPostCreatedAt(): ?\DateTimeInterface
    {
        return $this->postCreatedAt;
    }

    // public function setPostCreatedAt(\DateTimeInterface $postCreatedAt): self
    // {
    //     $this->postCreatedAt = $postCreatedAt;

    //     return $this;
    // }

    public function getPostUpdatedAt(): ?\DateTimeInterface
    {
        return $this->postUpdatedAt;
    }

    // public function setPostUpdatedAt(?\DateTimeInterface $postUpdatedAt): self
    // {
    //     $this->postUpdatedAt = $postUpdatedAt;

    //     return $this;
    // }

    public function getUser(): ?Users
    {
        return $this->user;
    }

    public function setUser(?Users $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Categories[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(Categories $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function removeCategory(Categories $category): self
    {
        $this->category->removeElement($category);

        return $this;
    }

    /**
     * @return Collection|Comments[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comments $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setPost($this);
        }

        return $this;
    }

    public function removeComment(Comments $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getPost() === $this) {
                $comment->setPost(null);
            }
        }

        return $this;
    }
}
