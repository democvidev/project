<?php

namespace App\Form;

use App\Entity\Categories;
use App\Entity\Posts;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('postTitle', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ],
                'label' => 'Title'
            ])
            ->add('postContent', TextareaType::class, [
                'attr' => [
                    'class' => 'form-control'
                ],
                'label' => 'Content'
            ])
            ->add('postImage', FileType::class, [
                'attr' => [
                    'class' => 'form-control'
                ],
                'data_class' => null,
                'mapped' => false
            ])
            ->add('category', EntityType::class, [
                'class' => Categories::class,
                'label' => 'Categories',
                'multiple' => true,
                'expanded' => true,
                'attr' => [
                    'class' => 'd-flex justify-content-start align-items-center'
                ]
            ])
            // ->add('postSlug')
            // ->add('postCreatedAt')
            // ->add('postUpdatedAt')
            // ->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Posts::class,
        ]);
    }
}
